package pl.edu.pwsztar;

import java.util.Optional;

interface UserIdChecker {

    enum Sex {
        MAN, WOMAN
    }

    boolean isCorrectSize();

    Optional<Sex> getSex();

    boolean isCorrect();

    Optional<String> getDate();
}
