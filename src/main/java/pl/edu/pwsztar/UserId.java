package pl.edu.pwsztar;

import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        return id.length() == 11;
    }

    @Override
    public Optional<Sex> getSex() {
        int sexChar = id.charAt(9);
        Optional<Sex> sex;
        if(sexChar % 2 == 0) {
            sex = Optional.of(Sex.WOMAN);
        } else {
            sex = Optional.of(Sex.MAN);
        }
        return sex;
    }

    @Override
    public boolean isCorrect() {
        String regex = "\\d{11}";
        if (id.matches(regex)) {
        int[] sumControlNumbers = {9,7,3,1,9,7,3,1,9,7};
        int sum = 0;
        int controlNumber = Integer.parseInt(String.valueOf(id.charAt(10)));

        for(int i = 0; i < id.length()-1; i++) {
            sum += Integer.parseInt(String.valueOf(id.charAt(i))) * sumControlNumbers[i];
        }

        return sum % 10 == controlNumber;

        } else {
            return false;
        }
    }

    @Override
    public Optional<String> getDate() {
        return Optional.empty();
    }
}
