package pl.edu.pwsztar

import spock.lang.Specification

class UserIdSpec extends Specification {

    def "should check correct size"() {
        given:
            UserId userId = new UserId("59030122419")
        when:
            def isCorrectSizeId = userId.isCorrectSize()
        then:
            isCorrectSizeId
    }

    def "should return sex"() {
        given:
            UserId userId = new UserId("59030122419")
        when:
            def returnSex = userId.getSex().toString() == "Optional[MAN]"
        then:
            returnSex
    }

    def "should check correct"() {
        given:
            UserId userId = new UserId("59030122419")
        when:
            def isCorrect = userId.isCorrect()
        then:
            isCorrect
    }

    def "should return date"() {
        given:
            UserId userId = new UserId("59030122419")
        when:
            def returnDate = userId.getDate()
        then:
            returnDate
    }
}
